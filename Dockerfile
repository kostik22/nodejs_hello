FROM node

WORKDIR /opt/app

COPY app.js .

EXPOSE 3000

CMD ["node", "app.js"]


